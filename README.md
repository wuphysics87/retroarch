# retroarch

[Retroarch](https://www.retroarch.com/) is an retro game emulation framework that you can play classic games from the NES, SNES, Sega Genesis, and more.  This repo will detail how you can install retroarch as well as roms (the games themselves) on your own computer.

Let's get started.

## MacOS install
Open a terminal and run:

```brew install --cask retroarch```

## Windows install

Open a Powershell Admin Terminal, and run:

```choco install retroarch --force --ignore-checksums```
## Navigating Retroarch
By default, to go forward press `enter/return` and to go backwards, press `backspace` pressing `escape` twice will exit retroarch.
## Installing Cores

Retroarch is actually a wrapper for the emulators themselves allowing you to use one program to run all of your games.  In order to play games, you'll need the emulator cores.  To install them:

1. Open Retroarch
2. Click _Online Updater_ followedby _Core Downloader_
3. Install any of the following you are interested in:
    - Nintendo - NES / Famicom (QuickNES)
    - Nintendo - SNES / SFC  (Snex9x - Current)
    - Nintendo - Nintendo 64 (Mupen64Plus-Next)
    - Nintendo - Game Boy Advance (mGBA)
    - Nintendo - GameCube / Wii (Dolphin)
    - Sega - MS/GG/MD/CD (Genesis Plus GX Wide)
    - Sony - Playstation (Beetle PSX HW)
    - Sony PlayStation 2 (LRPS2)
    - Sony PlayStation Portable (PPSSPP)

## Getting ROMS

**Warning:** This is not _stricktly_ legal, but you can consider it to be legal in the the same sense of jaywalking. Continue at your own discretion.

You can manually download _EVERY_ game in existence for the above systems by going to [vimm.net](https://vimm.net/).

Alternatively, you can run `get-roms.bash` and follow the prompts to download all games for your desired system. This will take time as you will be literally downloading thousands of games

## Adding ROMS to retroarch

Once you've downloaded your roms, from retroarch, 

1. Click import content.
2. Then scan directory.
3. Navigate to your retroarch directory
4. Select Scan This directory.

All games in this directory will be added at the bottom of the main menu for their respective system.


## Adding thumbnails
From Main Menu
1. Click Online Updater
2. Then Playlist Thumbnail Downloads.
3. Then your the system for your roms

## Latency Reduction
1. Click Main Menu
2. Click Latency
3. Turn on Run-Ahead to REduce Latency
4. Change Number of Frames to Run-Ahead to 3

## Input Configuration
1. Main Menu
2. Input
3. Scroll down to Port 1 Controls
4. Rebind according to preferences

## Licensing and Legal

As stated, from the standpoint of companies who make these games, this is not strictly legal, but it is EXTRA not legal to sell anything from this process.  Additionally, the Free Software developers who created RetroArch as well as the emulator cores have licensed their software to be non comercial.  With respect for their hard work and desire to share it with others, it would be unethical to sell their work.  Rather pay it forward.  Don't try to sell it.  Share the love of retro gaming with your friends and family!