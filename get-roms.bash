#!/usr/bin/env bash

download_roms(){
    for ((i=$1; i<=$2; i++)); do
        cd "$HOME/retroarch"
        curl  -G -L "https://download3.vimm.net/download/?mediaId=$i" -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/110.0' -H 'Referer: https://vimm.net/' -O  -J
    done
}

choose_system(){
printf "\n============================================"
printf "\n NOTE: This Script has not been fully tested"
printf "\n       It may not work as expected"
printf "\n============================================\n"
printf "Download roms for which systems?
    1. NES
    2. SNES
    3. GameBoy
    4. N64
    5. GameCube
    6. Sega Genesis
    7. Playstation1-2
    8. Playstation Portable
    0. All\n : "

read -r system
    case $system in
        "1") download_roms 3      981    "NES";;
        "2") download_roms 983    1770   "SNES";;
        "3") download_roms 2955   5932   "GameBoy";;
        "4") download_roms 2465   2761   "N64";;
        "5") download_roms 7461   7634   "GameCube";;
        "6") download_roms 1771   2464   "Sega Genesis";;
        "7") download_roms 6071   9894   "Playstation1-2";;
        "9") download_roms 23991  23973  "Playstation Portabale";;
        "0") download_roms 1      100000 "All";;
    esac
}

mkdir -p "$HOME/retroarch"
choose_system